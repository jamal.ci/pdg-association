from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm


class UserForm(UserCreationForm):
    username = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Nom utilisateur', 'max_length': 50, 'required': 'required'}))

    email = forms.EmailField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Email', 'max_length': 50, 'required': 'required'}))

    last_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Nom', 'max_length': 50, 'required': 'required'}))

    first_name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Prenom', 'max_length': 50, 'required': 'required'}))

    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Mot de passe', 'max_length': 50, 'required': 'required'}))

    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Mot de passe', 'max_length': 50, 'required': 'required'}))

    class Meta:
        model = User
        fields = ("username", "email", "first_name",
                  "last_name", "password1", "password2",)
