
from django.urls import path
from . import views

app_name = 'pdg'
urlpatterns = [
    path('authentification/', views.registration, name='page_inscription'),

    # Route pour authentification
    path('login/', views.login_pdg, name='login'),
    path('registration/', views.registration_pdg, name='registration'),
    path('logout/', views.logout_pdg, name='logout'),
]
