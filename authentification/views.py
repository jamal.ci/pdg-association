#!/usr/bin/env python
# -*- coding: utf-8 -*-*
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from .forms import UserForm

from django.contrib import messages

# requis pour la connexion
# Decorateurs
from django.contrib.auth.decorators import login_required
from django.urls import reverse
# Login
from django.contrib.auth import login, authenticate, logout

# Create your views here.


def registration(request):
    form = UserForm()
    data = {
        'page': {
            'title': 'Authentification',
            'title_desc': 'Akwaba',
        },
        'form': form,
    }
    return render(request, 'registration.html', data)

# Fonction de registration login - logout
# Registration


def registration_pdg(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            # Connexion
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            # Message
            messages.success(
                request, 'Votre compte a été crée sur Association')

            return redirect('core:page_dashbord')
        

    else:
        form = UserForm()

    return render(request, 'registration.html', {'form': form})


# Login
def login_pdg(request):
    data = {}
    if request.method == 'POST':
        # Recuperation des donnees postees
        credential = request.POST.get('credential')
        password = request.POST.get('password')
        # Authentification
        user = authenticate(username=credential, password=password)
        if user:
            if user.is_active:
                login(request, user)
                messages.success(request, "Connexion reussie!")
                return redirect('core:page_dashbord')
            else:
                messages.warning(
                request, 'Compte Inactif')
        else:
            messages.warning(
                request, 'Nom utilisateur ou mot de passe invalide')

    return render(request, 'registration.html', data)


# Logout
@login_required(login_url="pdg:page_inscription")
def logout_pdg(request):
    logout(request)
    return HttpResponseRedirect(reverse("pdg:page_inscription"))
