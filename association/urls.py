
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='page_accueil'),
    path('contact', views.contact, name='page_contact'),

    path('pdg/', include('authentification.urls')),
    path('core/', include('core.urls')),

    path('admin/', admin.site.urls),
]
