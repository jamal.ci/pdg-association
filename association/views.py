#!/usr/bin/env python
# -*- coding: utf-8 -*-*
from django.shortcuts import render
from django.contrib import messages


def index(request):
    data = {
        'page': {
            'title': 'Bienvenue',
            'title_desc': 'Akwaba',
        },
    }
    return render(request, 'index.html', data)


def contact(request):
    data = {
        'page': {
            'title': 'Charabia',
            'title_desc': 'Akwaba',
        },
    }
    return render(request, 'contact.html', data)
