from django.urls import path
from . import views

app_name = 'core'
urlpatterns = [

     # Route des pages autorisées aux utilisateurs connectés
     path('tableau-de-bord/', views.dashbord_pdg, name='page_dashbord'),
     path('mon-profil/', views.profile_pdg, name='page_profile'),
     path('creation-de-groupe/', views.create_group_pdg, name='page_new_group'),
     path('rejoindre-un-groupe/', views.join_group_pdg, name='page_join_group'),
     path('mes-groupes/', views.groups_pdg, name='page_groups'),
     path('mon-groupe/<str:code>/detail/',
          views.group_detail_pdg, name='page_detail_group'),
     path('mes-messages/', views.messages_pdg, name='page_messages'),
     path('validation-en-attente/',
          views.atendance_validation_pdg, name='page_validation'),
     path('validation-en-attente/<str:id>/validate/', views.groupe_validation, name='page_validate'),
     path('configuration/', views.settings_pdg, name='page_settings'),
]
