#!/usr/bin/env python
# -*- coding: utf-8 -*-*
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect

from django.contrib import messages
from django.utils.safestring import mark_safe

# requis pour la connexion
# Decorateurs
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from .models import Groupe, Member
from django.db.models import Q

import uuid
import random

def shuffle_characters(srt):
    charlst = list(srt)        # convert the string into a list of characters
    random.shuffle(charlst)     # shuffle the list of characters randomly
    new_word = ''.join(charlst)
    return new_word

def get_auto_code():
    return uuid.uuid1().hex[:8].upper()


@login_required(login_url="pdg:page_inscription")
def dashbord_pdg(request):
    data = {
        'page': {
            'title': 'Tableau de bord',
            'dashbord_active': True,
        },
    }
    return render(request, 'core/dashbord.html', data)


@login_required(login_url="pdg:page_inscription")
def profile_pdg(request):
    data = {
        'page': {
            'title': 'Mon profil',
            'profile_active': True,
        },
    }
    return render(request, 'core/profile.html', data)


@login_required(login_url="pdg:page_inscription")
def create_group_pdg(request):

    if request.method == 'POST':
        # Recuperation des donnees postées
        nom = request.POST.get('name')
        code = get_auto_code()
        description = request.POST.get('description')
        montant_adhesion = request.POST.get('adhesion_price')
        montant_cotisation = request.POST.get('cotisation_price')

        # Controle de la validité des données
        if nom:
            # Sauvegarde de groupe dans la base de données
            
            while Groupe.objects.filter(code=code).exists():
                code = shuffle_characters(code)                
            
            groupePdg = Groupe.objects.create(
                name=nom, code=code, description=description, adhesion_price=montant_adhesion, cotisation_price=montant_cotisation)
            # Creation de adherent et inscrit le createur du groupe comme admin
            Member.objects.create(
                groupe=groupePdg, user=request.user, statut=True, is_admin=True, adhesion_price=groupePdg.adhesion_price)
            # Message de succes quand c'est enregistré
            messages.success(
                request, mark_safe(f"Votre groupe &laquo;<b> {nom} </b>  &raquo; a été créé !"))
            # Redirection sur la page contact
            return redirect('core:page_groups')

    data = {
        'page': {
            'title': 'Créer un groupe',
            'new_group_active': True,
        },
    }
    return render(request, 'core/new_group.html', data)


@login_required(login_url="pdg:page_inscription")
def join_group_pdg(request):
    # recuperer le code saisi
    codeSaisi = request.POST.get('code')
    # Controle de la validité des données
    if codeSaisi and len(codeSaisi) == 8:
        try:
            groupePdg = Groupe.objects.get(code=codeSaisi)
            if groupePdg:
                Member.objects.create(
                    groupe=groupePdg, user=request.user, adhesion_price=groupePdg.adhesion_price)
                messages.success(request, mark_safe(
                    f"Vous etes en attente de validation par l'admin du groupe &laquo;<b> {groupePdg} </b>  &raquo;!"))
            return redirect('core:page_join_group')

        except Groupe.DoesNotExist as notFound:
            messages.warning(request, mark_safe(f"Groupe introuvable!"))

            return redirect('core:page_join_group')
    elif codeSaisi:
        messages.warning(
            request, mark_safe(f"Code &laquo;<b> {codeSaisi} </b>  &raquo; invalide"))
    data = {
        'page': {
            'title': 'Rejoindre un groupe',
            'join_group_active': True,
        },
    }
    return render(request, 'core/join_group.html', data)


@login_required(login_url="pdg:page_inscription")
def groups_pdg(request):
    userGroups = Member.objects.filter(user=request.user.id, statut=True)
    data = {
        'page': {
            'title': 'Mes groupes',
            'groups_active': True,
        },
        'members': userGroups,
    }
    return render(request, 'core/groups.html', data)


@login_required(login_url="pdg:page_inscription")
def group_detail_pdg(request, code):          
    try:
        #groupePdg = Groupe.objects.get(code=code)
        groupePdg = Groupe.objects.get(code=code)
        liste_membres_groupe = Member.objects.filter(Q(groupe=groupePdg.id), Q(statut=True))
                
    except(KeyError, Groupe.DoesNotExist, NameError):
        #Reafficher le formulaire de vote
        messages.warning(request, mark_safe(f"Groupe introuvable!"))   
        return redirect('core:page_groups') 

    data = {
        'page': {
            'title': 'Detail du groupe',
            'groups_active': True,
        },
        'groupe': groupePdg,
        'members': liste_membres_groupe,
    }
    return render(request, 'core/detail_group.html', data)


@login_required(login_url="pdg:page_inscription")
def atendance_validation_pdg(request):
    #userGroups = Member.objects.filter(user=request.user.id, statut=False)
    #userGroups = Member.objects.filter(~Q(user_id=request.user.id) , Q(statut=False))
    #member = Member.objects.filter(user=request.user).filter(is_admin=True).filter(status=False)
    #groupsID = [_.groupe.id  for _ in member ]
    #userGroups = Member.objects.filter(**{'user': request.user, 'is_admin':True, 'statut': False})
    #userGroups = Member.objects.exclude(user_id=request.user.id).filter(status=False)
    #userGroups = Member.objects.filter(~Q(user_id=request.user.id) & Q(statut=False) & Q(is_admin=False))
    
    member = Member.objects.filter(user=request.user).filter(is_admin=True)
    groupsID = [_.groupe.id  for _ in  member ]
    userGroups = Member.objects.filter(groupe__in=groupsID).filter(statut=False).exclude(user=request.user)
    #userGroups = Member.objects.filter(groupe__in=groupsID)
    
    print(userGroups)
    
    
    data = {
        'page': {
            'title': 'Validations en attente',
            'validation_active': True,
        },
        'members': userGroups,
    }
    return render(request, 'core/validation.html', data)


@login_required(login_url="pdg:page_inscription")
def messages_pdg(request):
    data = {
        'page': {
            'title': 'Mes messages',
            'messages_active': True,
        },
    }
    return render(request, 'core/messages.html', data)


@login_required(login_url="pdg:page_inscription")
def settings_pdg(request):
    data = {
        'page': {
            'title': 'Configuration',
            'settings_active': True,
        },
    }
    return render(request, 'core/settings.html', data)

def groupe_validation(request, id):
    """ ToDo: notifier aux demandeurs (email, SMS) """
    #Member.objects.filter(pk=survey.pk).update(active=True)
    membre = Member.objects.get(pk=id)
    membre.statut = True
    membre.save()
    messages.success(request, mark_safe(
                f"La demande d'adhesion a été validée avec succès !"))
    return redirect('core:page_validation')
