from django.db import models
from django.contrib.auth.models import User
import uuid

# Create your models here.


def get_auto_code():
    return uuid.uuid1().hex[:8].upper()


class Groupe(models.Model):
    name = models.CharField(max_length=128)
    description = models.TextField(null=True)
    #code = models.CharField(max_length=8, unique=True, default=get_auto_code())
    code = models.CharField(max_length=8, unique=True)
    adhesion_price = models.IntegerField(null=True, blank=True)
    cotisation_price = models.IntegerField(null=True, blank=True)
    add = models.DateTimeField(auto_now_add=True, verbose_name='Créer le')

    def __str__(self):
        return "%s" % self.name

    def member_count(self):
        return self.group_members.all().count()


# Adherent du groupe
class Member(models.Model):
    groupe = models.ForeignKey(
        Groupe, on_delete=models.CASCADE, related_name="group_members")
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="user_members")
    statut = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    add = models.DateTimeField(auto_now_add=True, verbose_name='Ajouté le')
    adhesion_price = models.IntegerField(null=True, blank=True)

    class Meta:
        constraints = [models.UniqueConstraint(
            fields=['groupe', 'user'], name='existante au groupe')]


class Event(models.Model):
    name = models.CharField(max_length=200)
    amount = models.PositiveIntegerField(default=0)
    start_date = models.DateField()
    end_date = models.DateField()
    received_amount = models.PositiveIntegerField(default=0)
    paid_amount = models.PositiveIntegerField(default=0)
    group_code = models.CharField(max_length=8, db_index=True)
    created_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return "%s" % self.name


class Meet(models.Model):
    date = models.DateField()
    amount = models.PositiveIntegerField(default=0)
    group_code = models.CharField(max_length=8, db_index=True)
    file = models.FileField(null=True)
    created_date = models.DateField(auto_now_add=True)


class Photo(models.Model):
    created_date = models.DateField(auto_now_add=True)
    meet = models.ForeignKey(Meet, on_delete=models.CASCADE)
    link = models.CharField(max_length=200)


class Contribution(models.Model):
    state = models.BooleanField(default=False)
    paiement_mode = models.CharField(max_length=100)
    reference = models.CharField(max_length=100)
    amount = models.IntegerField()
    presence = models.BooleanField(default=False)
    meet = models.ForeignKey(Meet, on_delete=models.CASCADE)
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    created_date = models.DateField(auto_now_add=True)


class CashType(models.Model):
    state = models.BooleanField(default=False)
    name = models.CharField(max_length=100)

    def __str__(self):
        return "%s" % self.name


class Cash(models.Model):
    amount = models.IntegerField(default=0)
    member = models.ForeignKey(Member, on_delete=models.CASCADE, db_index=True)
    type_cash = models.ForeignKey(CashType, on_delete=models.CASCADE)


class Operation(models.Model):
    operation_tag = {
        ("entrance", "Entrée"),
        ("out", "Sortie"),
    }
    name = models.CharField(max_length=200)
    description = models.TextField()
    type = models.CharField(max_length=8, choices=operation_tag)
    date = models.DateField()
    created_date = models.DateField(auto_now_add=True)

    def __str__(self):
        return "%s" % self.name

# Adhesion


class MemberShip(models.Model):
    date = models.DateField()
    amount = models.IntegerField(default=0)
    group_code = models.CharField(max_length=8, db_index=True)
    member = models.ForeignKey(Member, on_delete=models.CASCADE)


class EventMember(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    created_date = models.DateField(auto_now_add=True)
